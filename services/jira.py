"""
Jira

Supports synchronising issues and sprints.
"""

import aiohttp
import asyncio as aio
import logging
from queue import Queue
from datetime import datetime, timedelta
from threading import Thread
from base64 import standard_b64encode
from typing import List, NamedTuple
from dateutil.parser import parse
from entities.sprints import Sprint
from entities.boards import BoardItem

LOG = logging.getLogger('pbs.jira')


class JiraSprintData(NamedTuple):
    """Jira Agile (ex Greenhopper) sprint data."""
    id: int
    name: str
    start_date: datetime
    end_date: datetime
    state: str
    rapid_view_id: int = None
    goal: str = None
    sequence: int = None
    complete_date: datetime = None


class JiraIssue(BoardItem):
    """"""
    def __init__(self, data, points_field='', sprint_field=''):
        self.id = data['id']
        self.points_field = points_field
        self.sprint_field = sprint_field
        self.fields = data['fields']
        self.points = data['fields'].get(points_field) or 0
        self.url = data.get('self')
        self.key = data.get('key')
        self.updated = Jira.to_datetime(data['fields'].get('updated'))

    def sprints(self) -> List[JiraSprintData]:
        sprints = []
        for data in self.fields.get(self.sprint_field) or []:
            s = {}

            for kv in data[data.find('[') + 1: -1].split(','):
                k, v = kv.split('=')
                s[k] = int(v) if v.isdigit() else v

            sprints.append(
                JiraSprintData(id=s.get('id'),
                               name=s.get('name'),
                               state=s.get('state'),
                               start_date=Jira.to_datetime(s.get('startDate') or None),
                               end_date=Jira.to_datetime(s.get('endDate') or None)))
        return sprints

    def sprint_data(self, first=True, active=True) -> JiraSprintData:
        sprints = self.sprints()
        data = (s for s in sprints if first and active and s.state == 'ACTIVE')
        return next(data)

    def completed_on(self, date: datetime):
        if date is None:
            return True
        else:
            return self.is_completed and self.updated.day == date.day

    @property
    def is_completed(self) -> bool:
        return self.fields['status']['name'] in ['Done', 'Deployed']

    @property
    def is_in_sprint(self) -> bool:
        for data in self.fields.get(self.sprint_field) or []:
            if 'state=ACTIVE' in data:
                return True

        return False


class JiraSprint(Sprint):
    def __init__(self, data: JiraSprintData, points_planned: float):
        super().__init__()
        self.id = data.id
        self.starts = data.start_date
        self.ends = data.end_date
        self.points_planned = points_planned
        self.points_total = points_planned
        self.points_done = [0] * len(self)


class Jira:
    def __init__(self, cfg, loop: aio.AbstractEventLoop=None, q_in: Queue=None, q_out: Queue=None):
        self.url = cfg.get('Url', 'https://jira.atlassian.com')
        self.token = cfg.get('Token')
        self.username = cfg.get('User')
        self.password = cfg.get('Pass')
        self.freq = cfg.getint('Freq', 300)
        self.query = cfg.get('Jql')
        self.points_field = cfg.get('PointsField')
        self.sprint_field = cfg.get('SprintField')
        self.should_sync_issues = cfg.getboolean('SyncIssues', False)
        self.should_sync_sprint = cfg.getboolean('SyncSprint', True)
        self.issues: List[JiraIssue] = []

        headers = {}
        if self.token:
            headers['Authorization'] = f'Bearer {self.bearer_token}'
        else:
            headers['Authorization'] = f'Basic {self.basic_digest}'

        self.q_in = q_in
        self.q_out = q_out
        self.loop = loop or aio.new_event_loop()
        thread = Thread(target=self._start_loop)
        thread.daemon = True
        thread.start()

        self.session = aiohttp.ClientSession(loop=self.loop, headers=headers)
        aio.run_coroutine_threadsafe(self.start(), loop=self.loop)

    def __exit__(self, *e):
        self.session.close()
        self.loop.call_soon_threadsafe(self.loop.stop)
        self.loop.close()

    def _start_loop(self):
        aio.set_event_loop(self.loop)
        self.loop.run_forever()

    @staticmethod
    def to_datetime(date: str) -> datetime:
        if date:
            return parse(date)

    @property
    def basic_digest(self):
        credentials = f'{self.username}:{self.password}'.encode()
        return standard_b64encode(credentials).decode()

    @property
    def bearer_token(self):
        return ''

    @property
    def points(self):
        return sum([issue.points for issue in self.issues])

    # TODO: maybe impl this type of thing as a filter for 'points'?
    def points_done(self, date: datetime=None):
        return sum([issue.points for issue in self.issues if issue.is_completed and issue.completed_on(date)])

    @property
    def sprint(self) -> JiraSprint:
        issue = next((i for i in self.issues if i.is_in_sprint))
        sprint_data = issue.sprint_data()
        sprint = JiraSprint(sprint_data, self.points)
        for day_idx in range(len(sprint)):
            day = sprint.starts + timedelta(days=day_idx)
            sprint.points_done[day_idx] = self.points_done(day)
        return sprint

    async def fetch_issues(self) -> bool:
        """Fetch all issues from Jira and return True if there was a change in total points done."""
        url = f'{self.url}/rest/api/2/search'
        params = [('jql', self.query), ('fields', '*all')]
        async with self.session.get(url, params=params) as res:
            if res.status < 300:
                data = await res.json()
                points_done_before = self.points_done()
                self.issues = [JiraIssue(x, self.points_field, self.sprint_field) for x in data['issues']]
                points_done_after = self.points_done()
                return points_done_before != points_done_after or points_done_before == points_done_after == 0
            else:
                data = await res.text()
                LOG.error(data)
                return False

    #
    # not used for now
    #
    async def fetch_sprint(self):
        url = f'{self.url}/rest/agile/1.0/sprint/{self.sprint.id}/issue'
        async with self.session.get(url) as res:
            if res.status < 300:
                sprint = await res.json()
                return sprint
            else:
                data = await res.text()
                LOG.error(data)

    async def start(self):
        LOG.info('service started')
        while True:
            if self.should_sync_sprint:
                updated = await self.fetch_issues()
                if updated:
                    LOG.debug('sprint sync event')
                    sprint = self.sprint
                    self.q_out.put_nowait(sprint)

            await aio.sleep(self.freq)
