"""
Trello
"""
import io
import asyncio as aio
import aiohttp
import matplotlib.pyplot as plt
import logging
from typing import List
from queue import Queue, Empty
from threading import Thread
from PIL import Image
from entities.colors import Colors
from entities.sprints import Sprint
from entities.boards import BoardItem

LOG = logging.getLogger('pbs.trello')


class Trello:
    def __init__(self, cfg, loop: aio.AbstractEventLoop=None, q_in: Queue=None, q_out: Queue=None):
        self.url = cfg.get('Url')
        self.key = cfg.get('Key')
        self.token = cfg.get('Token')
        self.freq = cfg.getint('Freq', 300)
        self.board = cfg.get('Board')
        self.member = cfg.get('Member')
        self.should_sync_cards = cfg.getboolean('SyncCards', False)
        self.should_sync_sprint = cfg.getboolean('SyncSprint', True)
        self.background_fn = cfg.get('Background', None)
        self.background_id = None
        self.cards = {}

        self.q_in = q_in
        self.q_out = q_out
        self.loop = loop or aio.new_event_loop()
        thread = Thread(target=self._start_loop)
        thread.start()

        self.session = aiohttp.ClientSession(loop=self.loop)

        if self.background_fn:
            self.background = Image.open(f'backgrounds/{self.background_fn}')
        else:
            aio.run_coroutine_threadsafe(self.fetch_background(), loop=self.loop)

        aio.run_coroutine_threadsafe(self.start(), loop=self.loop)

    def __exit__(self, *e):
        self.session.close()
        self.loop.call_soon_threadsafe(self.loop.stop)
        self.loop.close()

    def _start_loop(self):
        aio.set_event_loop(self.loop)
        self.loop.run_forever()

    def background_with_burndown(self, planned: List[float], actual: List[float]) -> bytes:
        """Overlay a burndown chart over the background image."""

        # TODO: move this to the Colors class
        red = tuple(x/255 for x in Colors.red)
        lred = tuple(x/255 for x in Colors.light_red)

        # style plot
        plt.figure(figsize=(11, 5))
        ax = plt.subplot(111)
        plt.axis('off')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.spines["top"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["left"].set_visible(False)

        # get x-axis values
        l_days = [x - 0.5 for x in range(1, len(planned) + 1)]
        b_days = [x for x in range(1, len(planned) + 1)]

        # build line plot shadow
        plt.plot(l_days,
                 planned,
                 zorder=10,
                 solid_capstyle='round',
                 solid_joinstyle='round',
                 color='black',
                 alpha=0.20,
                 antialiased=True,
                 linewidth=15)

        # build line plot
        plt.plot(l_days,
                 planned,
                 zorder=10,
                 solid_capstyle='round',
                 solid_joinstyle='round',
                 color=red,
                 alpha=0.85,
                 antialiased=True,
                 linewidth=5)

        # build bar plot
        for bar in plt.bar(b_days, actual, zorder=20, width=0.75, color=lred + (0.85,)):
            height = bar.get_height()
            if height > 8:
                plt.text(bar.get_x() + bar.get_width()/2.,
                         height - 4,
                         '%d' % int(height),
                         zorder=30,
                         fontsize=16,
                         weight='bold',
                         ha='center',
                         va='bottom',
                         color='white')

        # save plot as image
        bd = io.BytesIO()
        plt.savefig(bd, dpi=200, transparent=True, format='png')

        # paste the plot image into the background image
        background = self.background.copy()
        burndown = Image.open(bd)
        bgw, bgh = background.size
        bdw, bdh = burndown.size
        position = (bgw - bdw, bgh - bdh)
        background.paste(burndown, position, burndown)

        out = io.BytesIO()
        background.save(out, format='png')
        return out.getvalue()

    async def fetch_background(self):
        """Retrieve the public image URL from the Trello board."""
        params = {'key': self.key, 'token': self.token}
        async with self.session.get(f'{self.url}/boards', params=params) as res:
            url = await res.json()['prefs']['backgroundImage']

        # fetch the image data
        async with self.session.get(url) as res:
            data = await res.read()
            stream = io.BytesIO(data)
            self.background = Image.open(stream)

    async def upload_background(self, background: bytes):
        """Upload the background to Trello and return background ID."""
        url = f'{self.url}/members/{self.member}/customBoardBackgrounds'
        data = aiohttp.FormData()
        data.add_field('key', self.key)
        data.add_field('token', self.token)
        data.add_field('file', background, filename=self.background_fn, content_type='image/jpeg')
        async with self.session.post(url, data=data) as res:
            if res.status < 300:
                background = await res.json()
                self.background_id = background['id']
            else:
                err = await res.text()
                LOG.error(err)

    async def update_board_background(self):
        """Set the board background."""
        url = f'{self.url}/boards/{self.board}'
        params = {'key': self.key, 'token': self.token, 'prefs/background': self.background_id}
        async with self.session.put(url, params=params) as res:
            if res.status >= 400:
                err = await res.text()
                LOG.error(err)

    async def delete_board_background(self, background_id):
        """Delete a board background."""
        url = f'{self.url}/members/{self.member}/customBoardBackgrounds/{background_id}'
        params = {'key': self.key, 'token': self.token}
        async with self.session.delete(f'{url}', params=params) as res:
            if res.status >= 400:
                err = await res.text()
                LOG.error(err)

    async def delete_old_board_backgrounds(self):
        """Delete unused board backgrounds."""
        params = {'key': self.key, 'token': self.token}
        LOG.debug('deleting old backgrounds')
        try:
            url = f'{self.url}/members/{self.member}/customBoardBackgrounds'
            async with self.session.get(url, params=params) as res:
                backgrounds = await res.json()

            for background in backgrounds:
                is_unused = background['id'] != self.background_id
                is_managed = background['fullSizeUrl'].endswith(self.background_fn)
                if is_unused and is_managed:
                    await self.delete_board_background(background['id'])
        except Exception as e:
            LOG.error(e)

    async def process_sprint(self, sprint):
        try:
            planned = [x['planned'] for x in sprint.burndown]
            actual = [x['actual'] for x in sprint.burndown]
            bg = self.background_with_burndown(planned, actual)
            await self.upload_background(bg)
            await self.update_board_background()
            await self.delete_old_board_backgrounds()
            LOG.info('sprint updated')
        except Exception as e:
            LOG.error(e)

    async def process_cards(self, item: BoardItem=None):
        url = f'{self.url}/boards/{self.board}/cards'
        params = {'key': self.key, 'token': self.token, 'fields': 'id,desc,labels'}
        async with self.session.get(url, params=params) as res:
            if res.status < 300:
                self.cards = await res.json()
            else:
                err = await res.text()
                LOG.error(err)

    async def start(self):
        LOG.info('service started')
        while True:
            try:
                item = self.q_in.get()
                if self.should_sync_sprint and issubclass(type(item), Sprint):
                    LOG.debug('sprint sync event')
                    await self.process_sprint(item)
                elif self.should_sync_cards and issubclass(type(item), BoardItem):
                    LOG.debug('board item sync event')
                    await self.process_cards(item)
                else:
                    LOG.debug(f'unknown item "{type(item)}" in queue')
            except Empty:
                pass

            await aio.sleep(self.freq)
