"""
Jira Tests
"""

import pytest
from services.jira import JiraIssue, JiraSprint, Jira


data = {
    'issues': [
        {
            'id': 1,
            'fields': {
                'customfield_10008': 1.0,
                'customfield_10003': [],
            }
        },
        {
            'id': 2,
            'fields': {
                'customfield_10008': 2.0,
                'customfield_10003': [
                    'com.atlassian.greenhopper.service.sprint.Sprint@6b44c1d2[id=131,rapidViewId=24,state=ACTIVE,name=AKL A Team: Sprint 48,goal=,startDate=2018-03-30T20:00:00.000Z,endDate=2018-04-03T23:30:00.000Z,completeDate=<null>,sequence=131]',
                ],
            },
        },
        {
            'id': 3,
            'fields': {
                'customfield_10008': 3.0,
                'customfield_10003': [
                    'com.atlassian.greenhopper.service.sprint.Sprint@6c1e88ff[id=128,rapidViewId=24,state=CLOSED,name=AKL A Team: Sprint 47,goal=Clear the backlog for grades,startDate=2018-02-16T03:00:07.945Z,endDate=2018-03-01T23:00:00.000Z,completeDate=2018-03-04T20:03:12.286Z,sequence=128]'
                ],
            },
        },
        {
            'id': 4,
            'fields': {
                'customfield_10008': 4.0,
                'customfield_10003': [
                    'com.atlassian.greenhopper.service.sprint.Sprint@6b44c1d2[id=131,rapidViewId=24,state=ACTIVE,name=AKL A Team: Sprint 48,goal=,startDate=2018-03-05T20:00:00.000Z,endDate=2018-03-15T23:30:00.000Z,completeDate=<null>,sequence=131]',
                    'com.atlassian.greenhopper.service.sprint.Sprint@6c1e88ff[id=128,rapidViewId=24,state=CLOSED,name=AKL A Team: Sprint 47,goal=Clear the backlog for grades,startDate=2018-02-16T03:00:07.945Z,endDate=2018-03-01T23:00:00.000Z,completeDate=2018-03-04T20:03:12.286Z,sequence=128]'
                ],
            },
        }
    ]
}


class TestJiraIssue:
    def test_sprints(self):
        issue = JiraIssue(data['issues'][3], 'customfield_10008', 'customfield_10003')
        sprints = issue.sprints()
        assert len(sprints) == 2
        assert sprints[0].id == 131

    def test_sprint(self):
        issue = JiraIssue(data['issues'][3], 'customfield_10008', 'customfield_10003')
        sprint = issue.sprint_data()
        assert sprint.id == 131


class TestJiraSprint:
    def test_init(self):
        issue = JiraIssue(data['issues'][3], 'customfield_10008', 'customfield_10003')
        sprint_data = issue.sprint_data()
        sprint = JiraSprint(sprint_data, 15)
        assert sprint.id == 131
        assert len(sprint) == 12
        assert len(sprint.points_done) == 12

    def test_idle_days(self):
        issue = JiraIssue(data['issues'][1], 'customfield_10008', 'customfield_10003')
        sprint_data = issue.sprint_data()
        sprint = JiraSprint(sprint_data, 15)
        idle = sprint._idle_days()
        assert len(sprint) == 6
        assert len(idle) == 4

    def test_burndown(self):
        issue = JiraIssue(data['issues'][3], 'customfield_10008', 'customfield_10003')
        sprint_data = issue.sprint_data()
        sprint = JiraSprint(sprint_data, 45)
        burndown = sprint.burndown
        assert len(burndown) == 12
        assert burndown[0]['planned'] == 45
        assert burndown[5]['planned'] == 20
        assert burndown[6]['planned'] == 20
        assert burndown[9]['planned'] == 10
        assert burndown[11]['planned'] == 0

    # def test_claim_points(self):
    #     issue = JiraIssue(data['issues'][3])
    #     sprint_data = issue.sprint()
    #     sprint = JiraSprint(sprint_data, 10)
    #     sprint.claim_points(5, 2)
    #     # sprint.claim_points(3)
    #     assert len(sprint) == 11
    #     assert sprint.points_done[2] == 5
    #     # assert sprint.points_done[10] == 3
    #
    # def test_increase_scope(self):
    #     issue = JiraIssue(data['issues'][3])
    #     sprint_data = issue.sprint()
    #     sprint = JiraSprint(sprint_data, 10)
    #     sprint.increase_scope(5, 2)
    #     assert len(sprint) == 11
    #     assert sprint.points_done[2] == -5


class TestJira:
    def test_extract_sprint_data(self):
        pass
        # sprint = Jira.parse_sprint_data(data)
        # assert sprint.id == 131

    def test_parse_sprint_data(self):
        pass
        # sprint = Jira.parse_sprint_data(data)
        # sprint = Jira.parse_sprint_data(data)
        # assert sprint.id == 131
