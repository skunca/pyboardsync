"""
Sprints

Sprint objects 
"""

from datetime import datetime, timedelta
from typing import List, Dict
import holidays
from entities.boards import BoardItem

COUNTRY = 'NZ'
PROVINCE = 'AUK'


class Sprint:
    id: str
    starts: datetime
    ends: datetime
    points_planned: int
    points_total: int
    points_done: List[int]
    holidays: holidays.HolidayBase

    def __init__(self):
        country_holidays = getattr(holidays, COUNTRY)
        self.holidays: holidays.HolidayBase = country_holidays()
        self.holidays.prov = PROVINCE
        self.points_total = 0

    def __len__(self) -> int:
        return (self.ends - self.starts).days + 2

    def _idle_days(self):
        """Get indexes of days that will see no work done."""
        indexes = []

        for idx in range(len(self)):
            date = self.starts.date() + timedelta(days=idx)
            if date.weekday() > 4 or date in self.holidays:
                indexes.append(idx)

        return indexes

    def update(self, points_total, points_done, day=None):
        """Update the sprint to reflect how much work was done."""
        if day is None:
            now = (datetime.now() - self.starts).days + 1
            day = min(len(self), now)

        if points_done > self.points_done_today:
            self.points_done[day] = points_done
            return self.burndown

        # adjust the daily totals and sprint scope
        if points_total != self.points_total:
            self.points_done[day] -= points_total - self.points_total
            self.points_total = points_total
            return self.burndown

    @property
    def points_done_today(self):
        """Check how many points were done today."""
        day = (datetime.now() - self.starts).days
        return 0 if day > len(self) else self.points_done[day]

    @property
    def burndown(self) -> List[Dict]:
        """Get the ideal and real values for a burndown chart."""
        idle = self._idle_days()
        ppd = self.points_planned / (len(self) - len(idle) - 1)
        points = self.points_planned
        progress = self.points_planned
        data = []
        days_till_end = (datetime.utcnow() - self.starts.replace(tzinfo=None)).days

        for idx in range(len(self)):
            if days_till_end > idx - 1:
                progress -= self.points_done[idx]
            else:
                progress = 0

            data.append({
                'planned': round(points, 2),
                'actual': round(progress, 2)
            })

            if idx not in idle:
                points -= ppd

        # make sure we end with a 0
        data[-1]['planned'] = 0
        return data
