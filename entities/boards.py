"""
Boards

Data classes representing actionable items and their collections.
"""

from typing import List
from entities.people import Person


# @dataclass
class BoardItem:
    """A single item on a board."""
    id: str
    name: str
    description: str
    points: float
    parent_id: str
    is_sub: bool
    assigned_to: List[Person]


# @dataclass
class Board:
    """A board with a collection of items and teams."""
    id: str
    name: str
    description: str
    items: List[BoardItem]
