"""
Colors
"""

# @dataclass
class Colors:
    blue = (31, 119, 180)
    orange = (255, 127, 14)
    green = (44, 160, 44)
    red = (214, 39, 40)
    purple = (148, 103, 189)
    brown = (140, 86, 75)
    pink = (227, 119, 194)
    grey = (127, 127, 127)
    yellow = (188, 189, 34)
    cyan = (23, 190, 207)
    light_orange = (255, 187, 120)
    light_blue = (174, 199, 232)
    light_green = (152, 223, 138)
    light_red = (255, 152, 150)
    light_purple = (197, 176, 213)
    light_brown = (196, 156, 148)
    light_pink = (247, 182, 210)
    light_grey = (199, 199, 199)
    light_yellow = (219, 219, 141)
    light_cyan = (158, 218, 229)
