"""
People

foo, bar
"""

from typing import List


# @dataclass
class Person:
    id: str
    first_name: str
    last_name: str


# @dataclass
class Team:
    id: str
    name: str
    people: List[Person]
    