# Task Board Sync

[![pipeline status](https://gitlab.com/skunca/pyboardsync/badges/master/pipeline.svg)](https://gitlab.com/skunca/pyboardsync/commits/master)
[![coverage report](https://gitlab.com/skunca/pyboardsync/badges/master/coverage.svg)](https://gitlab.com/skunca/pyboardsync/commits/master)

This project syncs board items between tasking services like Jira or Trello.

## Setup

The project uses [pipenv](https://docs.pipenv.org) for dep management. To
run the project:

    $ pipenv run python pbs

## Configuration

Copy `examples/config.ini` to the root of the project and adjust as needed.
Secrets can be passed through cli arguments:

    $ pbs jira --jira-user foo --jira-pass bar

or env vars:

    JIRA_USER
    JIRA_PASS
    TRELLO_KEY
    TRELLO_TOKEN

Env vars can be placed into a `.env` file which will be picked up by pipenv.
